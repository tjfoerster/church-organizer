# church-organizer

## About
Church-organizer is a tool for churches and non-profit organisations to manage everyday business. 

Our vision in 3 points:
- empower teams to do their actual work better
- remove annoying organizational work from leaders
- enable employees and group leaders to take responsibility for their groups

Technically, the web application is based on the [T3 stack](https://create.t3.gg/). It is a web development stack focused on simplicity, modularity, and full-stack typesafety. Core technologies are NextJS, Typescript and tRPC.

Learn more about our project
- [Features](https://gitlab.com/tjfoerster/church-organizer/-/wikis/Features)
- [Project wiki](https://gitlab.com/tjfoerster/church-organizer/-/wikis/home)
- [T3 Docs](https://create.t3.gg/en/introduction)

## Getting started


## Roadmap
- [TODO list](https://gitlab.com/tjfoerster/church-organizer/-/boards/5130448)
- [Milestones](https://gitlab.com/tjfoerster/church-organizer/-/milestones)

## License
For open source projects, say how it is licensed.

## Project status
Active, development by volunteers
